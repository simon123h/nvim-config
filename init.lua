-- Include configuration files
require("autocmds")
require("mappings")
require("settings")
require("plugins")

vim.cmd.colorscheme("tokyonight")

-- TODO:
-- https://github.com/ggandor/leap.nvim
-- https://github.com/LazyVim/LazyVim
-- https://github.com/nvim-lualine/lualine.nvim/issues/940
-- https://github.com/nvim-telescope/telescope.nvim/issues/1048
-- https://github.com/nvim-telescope/telescope.nvim/issues/2766
-- https://github.com/nvim-telescope/telescope.nvim/issues/2785
-- Include my keymaps into Legendary (or similar)

-- Tweak Cursorword hightlights
vim.api.nvim_set_hl(0, "MiniCursorword", { bg = "none", underline = true })
vim.api.nvim_set_hl(0, "MiniCursorwordCurrent", { bg = "none", underline = true })
