local autocmd = vim.api.nvim_create_autocmd

-- automatically rebalance windows on vim resize
autocmd({ "VimResized" }, {
	callback = function()
		vim.cmd("tabdo wincmd =")
	end,
})
-- automatically start insert mode when opening a git commit message
vim.cmd("autocmd FileType gitcommit exec 'au VimEnter * startinsert'")
-- Auto-format on save (markdown only)
autocmd("BufWritePre", {
	pattern = { "*.md", "*.lua" },
	callback = function()
		vim.lsp.buf.format({ async = false, timeout_ms = 3000 })
	end,
})

---Highlight yanked text
autocmd("TextYankPost", {
	callback = function()
		vim.highlight.on_yank()
	end,
})
