local present, null_ls = pcall(require, "null-ls")
if not present then
	return
end

null_ls.setup({
	sources = {
		-- null_ls.builtins.formatting.stylua,
		-- null_ls.builtins.formatting.prettier,
		-- null_ls.builtins.formatting.autopep8,
		-- null_ls.builtins.formatting.black,
		-- null_ls.builtins.formatting.isort,
		null_ls.builtins.formatting.fish_indent,
		-- null_ls.builtins.formatting.latexindent,
		-- null_ls.builtins.diagnostics.chktex,
		-- null_ls.builtins.diagnostics.markdownlint,
	},
})

local present, mason_null_ls = pcall(require, "mason-null-ls")
if not present then
	return
end

mason_null_ls.setup({
	ensure_installed = {
		"black",
		-- "markdownlint",
		"prettierd",
		"stylua",
	},
	automatic_installation = true,
	automatic_setup = true,
	handlers = {},
})
