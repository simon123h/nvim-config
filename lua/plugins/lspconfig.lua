local present, lspconfig = pcall(require, "lspconfig")
if not present then
	return
end

local present, mason = pcall(require, "mason")
if not present then
	return
end

-- configure mason.nvim
mason.setup()

local present, mason_lspconfig = pcall(require, "mason-lspconfig")
if not present then
	return
end

-- which LSPs are install depends on the hostname
local ensure_installed = {
	-- "pyright",
}
local utils = require("utils")
if utils.is_T14 then
	-- table.insert(ensure_installed, "marksman")
	table.insert(ensure_installed, "clangd")
	table.insert(ensure_installed, "lua_ls")
end

-- configure mason-lspconfig
mason_lspconfig.setup({
	ensure_installed = ensure_installed,
	automatic_installation = true,
})

-- configure some language servers
-- lspconfig["clangd"].setup({})
-- lspconfig["pyright"].setup({})
-- lspconfig["marksman"].setup({})

-- automatically setup installed servers
require("mason-lspconfig").setup_handlers({
	-- The first entry (without a key) will be the default handler
	-- and will be called for each installed server that doesn't have
	-- a dedicated handler.
	function(server_name) -- default handler (optional)
		require("lspconfig")[server_name].setup({})
	end,
	-- Next, you can provide targeted overrides for specific servers.
	["lua_ls"] = function()
		lspconfig.lua_ls.setup({
			settings = {
				Lua = {
					diagnostics = {
						globals = { "vim", "use", "require" },
					},
				},
			},
			on_attach = function(client)
				client.server_capabilities.documentFormattingProvider = false
			end,
		})
	end,
	["pyright"] = function()
		lspconfig.pyright.setup({
			settings = {
				python = {
					analysis = {
						typeCheckingMode = "basic",
					},
				},
			},
			on_attach = function(client)
				client.server_capabilities.documentFormattingProvider = false
			end,
		})
	end,
})
