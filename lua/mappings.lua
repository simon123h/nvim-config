local map = vim.keymap.set -- abbreviation for vim.keymap.set

-- Leader key, used for many keybindings
vim.g.mapleader = " "

-- Remap kj to Esc key
-- map('i', 'kj', '<Esc>')
map("v", "kj", "<Esc>")
-- map('t', 'kj', '<Esc>')

-- move cursor over wrapped displaylines
map("n", "<up>", "g<up>")
map("n", "<down>", "g<down>")
map("i", "<up>", "<C-o>g<up>")
map("i", "<down>", "<C-o>g<down>")

-- Close current buffer without closing window
map("", "<leader>w", function()
	require("mini.bufremove").delete()
end)
-- map("", "<leader>w", "<cmd>bd<cr>")
-- Close the current window
map("", "<leader>q", "<cmd>q<cr>")
-- Save file
map("", "<leader>s", "<cmd>update<cr>")
map("", "<C-s>", "<cmd>update<cr>")
map("i", "<C-s>", "<Esc><cmd>update<cr>")
map("v", "<C-s>", "<Esc><cmd>update<cr>")
-- map('i', '<C-s>', '<C-O><cmd>update<cr>')
-- Toggle line number modes
map("", "<F4>", "<cmd>exe 'set nu!' &nu ? 'rnu!' : ''<cr>")
-- Run current file, if it is executable
-- map("", "<F5>", '<cmd>!cd "%:p:h"; "./%:t"<cr>')
map("", "<leader><F5>", '<cmd>!cd "%:p:h"; chmod +x "%:t"; "./%:t"<cr>')
local f5term = nil
vim.keymap.set("", "<F5>", function()
	local fdir = vim.fn.expand("%:p:h")
	local fname = vim.fn.expand("%:t")
	if f5term == nil then
		local Terminal = require("toggleterm.terminal").Terminal
		f5term = Terminal:new({ hidden = true, close_on_exit = true })
	end
	if not f5term:is_open() then
		f5term:open()
	else
		f5term:send(vim.api.nvim_replace_termcodes("<C-c>clear", true, true, true), true)
	end
	f5term:change_dir(fdir)
	f5term:send('"./' .. fname .. '"', true)
end)
-- Make Ctrl+Z work also in insert mode
map("i", "<C-z>", "<C-o><C-z>")

-- Switch between buffers
map("", "ä", "<cmd>bnext<cr>") -- :bnext
map("", "ö", "<cmd>bprev<cr>") -- :bprev
-- Switch between tabs
map("", "Ä", "<cmd>tabnext<cr>")
map("", "Ö", "<cmd>tabprev<cr>")

-- text selection shortcuts: arrow keys and backspace
map("n", "<S-up>", "v<Up>")
map("n", "<S-Down>", "v<Down>")
map("n", "<S-Left>", "v<Left>")
map("n", "<S-Right>", "v<Right>")
map("v", "<S-Up>", "<Up>")
map("v", "<S-Down>", "<Down>")
map("v", "<S-Left>", "<Left>")
map("v", "<S-Right>", "<Right>")
map("i", "<S-Up>", "<Esc>v<Up>")
map("i", "<S-Down>", "<Esc>`^v<Down>")
map("i", "<S-Left>", "<Esc>v")
map("i", "<S-Right>", "<Esc>`^v")
map("v", "<Backspace>", '"_d')
map("n", "K", "v<Up>")
map("n", "J", "v<Down>")
map("n", "H", "v<Left>")
map("n", "L", "v<Right>")
map("v", "K", "<Up>")
map("v", "J", "<Down>")
map("v", "H", "<Left>")
map("v", "L", "<Right>")

-- mark all, copy, cut and paste
-- Ctrl+A
map("", "<C-a>", "gggH<C-O>G")
map("i", "<C-a>", "<C-O>gg<C-O>gH<C-O>G")
map("c", "<C-a>", "<C-C>gggH<C-O>G")
map("o", "<C-a>", "<C-C>gggH<C-O>G")
map("s", "<C-a>", "<C-C>gggH<C-O>G")
map("x", "<C-a>", "<C-C>ggVG")
-- Ctrl+C, Ctrl+X, Ctrl+V
map("v", "<C-c>", '"+y')
map("v", "<C-x>", '"+x')
map("i", "<C-v>", '<Esc>"+pa')
-- map("", "<C-c>", 'V"+ygv<esc>')
-- map("n", "<C-v>", '"+P')
-- map("v", "<C-v>", '"_c<Esc>"+p')

-- make 'd' delete stuff, instead of cutting it to clipboard/register
map("n", "d", '"_d') -- commented out because it breaks which-key completion
map("v", "d", '"_d')
-- do not yank replaced text when pasting in visual mode
map("v", "p", '"0p')
-- Multiline insert from standard visual mode (not only from visual-block mode)
map("v", "I", "<C-v>I")

-- Ctrl+Arrows: move a line of text
-- map("n", "<C-Down>", "<cmd>m .+1<cr>==")
-- map("n", "<C-Up>", "<cmd>m .-2<cr>==")
map("i", "<C-Down>", "<Esc><cmd>m .+1<cr>==gi")
map("i", "<C-Up>", "<Esc><cmd>m .-2<cr>==gi")
-- map("v", "<C-Down>", ":m '>+1<cr>gv=gv")
-- map("v", "<C-Up>", ":m '<-2<cr>gv=gv")

-- indent block in visual mode with <Tab> (and reverse)
map("v", "<Tab>", ">gv")
map("v", "<S-Tab>", "<gv")
map("i", "<S-Tab>", "<C-O>V<")
map("n", "<S-Tab>", "<<")
map("n", "<Tab>", ">>")

-- Clear search highlighting
-- map("n", "<leader>h", "<cmd>noh<cr><Esc>")
map({ "i", "n" }, "<esc>", "<cmd>noh<cr><esc>", { desc = "Escape and clear hlsearch" })
-- Clear search with <esc>
-- Search for selected text by hitting <*>
-- map("v", "*", 'y/<C-R>"<cr>N')

-- Prevent irreversible text deletion with Ctrl+u/w
map("i", "<C-u>", "<C-g>u<C-u>")
map("i", "<C-w>", "<C-g>u<C-w>")

-- Surround selection with brackets using vim-surround
map("v", "(", "sa)", { remap = true })
map("v", "[", "sa]", { remap = true })
map("v", "{", "sa}", { remap = true })
map("v", '"', 'sa"', { remap = true })
map("v", "'", "sa'", { remap = true })
map("v", "<", "sa>", { remap = true })

-- zoom a vim pane, <C-w>= to re-balance
map("n", "<leader>z", "<cmd>wincmd _<cr>:wincmd |<cr>")
map("n", "<leader>Z", "<cmd>wincmd =<cr>")

-- Ctrl+Alt+c to toggle Diff mode in a split view
map("n", "<C-M-c>", "<cmd>exe &diff ? 'diffoff!' : 'windo :diffthis | :set nofoldenable'<cr>")

-- Split view vertically
map("n", "<leader>5", "<cmd>vsp<cr><C-w><Left><cmd>bprev<cr><C-w><Right>")

-- Disable Ex mode and command line mode - I never use it
map("n", "Q", "<nop>")

-- Turn selected lines into a bullet list
map("v", "<leader>-", "0<C-v>I- <Esc>")
-- Insert the current date
map("", "<leader>n", "a<C-r>=strftime('%d.%m.%Y')<cr>")
-- move line to bottom of file
map("", "<leader>X", "VxGp")

-- LSP mappings: access all of LSP's features via <leader>l+<key>
map("n", "<leader>ff", "<cmd>lua vim.lsp.buf.format({async=true})<cr>")
map("n", "<leader>lö", vim.diagnostic.goto_prev)
map("n", "<leader>lä", vim.diagnostic.goto_next)
map("n", "<leader>la", vim.lsp.buf.code_action)
map("n", "<leader>lh", vim.lsp.buf.hover)
map("n", "<leader>lc", vim.lsp.buf.rename)
map("n", "<leader>ls", "<cmd>Telescope lsp_document_symbols<cr>")
map("n", "gd", "<cmd>Telescope lsp_definitions<cr>")
map("n", "<leader>lr", "<cmd>Telescope lsp_references<cr>")
map("n", "<leader>li", "<cmd>Mason<cr>")
map("n", "<leader>ll", "<cmd>Lazy<cr>")

-- Update packages with <leader>u
map("n", "<leader>u", "<cmd>Lazy sync<cr>")

-- open or reload vim config with <leader>,
map("", "<leader>,", function()
	-- change to settings directory, if no buffer open
	vim.cmd("if @% == '' | lcd ~/.config/nvim | end")
	-- open plugin files
	vim.cmd("args ~/.config/nvim/init.lua ~/.config/nvim/lua/*.lua")
end)
-- Clear cache and reload config
map("", "<leader>.", function()
	local files = { "autocmds", "mappings", "settings" }
	for _, name in ipairs(files) do
		package.loaded[name] = nil
	end
	dofile(vim.env.MYVIMRC)
	vim.notify("Config reloaded.")
end)

-- Toggle the color scheme
local themes = { "tokyonight-night", "tokyonight-day", "onedark" }
local theme_n = 1
map("", "<leader>c", function()
	theme_n = theme_n % #themes + 1
	vim.cmd.colorscheme(themes[theme_n])
	vim.notify("Colorscheme: " .. themes[theme_n])
end)

-- Exit the terminal using escape
map("t", "jk", "<C-\\><C-n>")
map("t", "kj", "<C-\\><C-n>")
map("t", "<C-w>", "<C-\\><C-n><C-w>")
map("t", "<C-h>", "<C-\\><C-n><C-w><left>")
map("t", "<C-j>", "<C-\\><C-n><C-w><down>")
map("t", "<C-k>", "<C-\\><C-n><C-w><up>")
map("t", "<C-l>", "<C-\\><C-n><C-w><right>")

-- open lazygit
local lazygit = nil
map("n", "<leader>g", function()
	if lazygit == nil then
		local Terminal = require("toggleterm.terminal").Terminal
		lazygit = Terminal:new({ cmd = "lazygit", direction = "float", hidden = true })
	end
	lazygit:toggle()
end)

-- move line to next window across windows
map("n", "<leader>m", 'V"0x<C-w><C-w>"0p<C-w><C-p>')
map("v", "<leader>m", '"0x<C-w><C-w>"0p<C-w><C-p>')
