-- Install lazy.nvim plugin manager if it is not installed
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"--single-branch",
		"https://github.com/folke/lazy.nvim.git",
		lazypath,
	})
end
vim.opt.runtimepath:prepend(lazypath)

-- Add plugins
require("lazy").setup({
	"nvim-lua/plenary.nvim", -- lua aux functions library, required for many plugins
	"nvim-tree/nvim-web-devicons", -- icon set
	{ "folke/todo-comments.nvim", event = { "BufReadPost", "BufNewFile" }, config = true }, -- Hightlighting and support for todo/note comments
	{ "chrishrb/gx.nvim", keys = { { "gx", "<cmd>Browse<cr>", mode = { "n", "x" } } }, opts = {} }, -- provide gx even when netrw is disabled
	{
		"kevinhwang91/nvim-hlslens",
		config = true,
		event = "VeryLazy",
	},
	{
		-- syntax detection & highlighting
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		event = { "BufReadPost", "BufNewFile" },
		dependencies = { "HiPhish/rainbow-delimiters.nvim" },
		config = function()
			require("nvim-treesitter.configs").setup({
				ensure_installed = {
					"bash",
					"c",
					"cpp",
					"diff",
					"fish",
					"gitcommit",
					"vimdoc",
					"html",
					"javascript",
					"json",
					"latex",
					"lua",
					"markdown",
					"markdown_inline",
					"python",
					"vim",
				},
				sync_install = false,
				auto_install = true,
				highlight = { enable = true },
				rainbow = { enable = true },
				incremental_selection = {
					enable = true,
					keymaps = {
						init_selection = "<CR>",
						node_incremental = "<CR>",
						scope_incremental = "<S-CR>",
						node_decremental = "<BS>",
					},
				},
			})
		end,
	},
	-- Telescope: fuzzy finding and more
	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			{ "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
		},
		opts = {
			defaults = { file_ignore_patterns = { "out" } },
		},
		config = function(_, opts)
			local telescope = require("telescope")
			telescope.setup(opts)
			telescope.load_extension("fzf")
		end,
		cmd = { "Telescope" },
		keys = {
			{ "<C-t>", "<cmd>Telescope<cr>", desc = "Open Telescope" },
			{ "<C-p>", "<cmd>Telescope find_files<cr>", desc = "Find files" },
			{ "<C-f>", "<cmd>Telescope current_buffer_fuzzy_find<cr>", desc = "Fuzzy find in current file" },
			{ "ff", "<cmd>Telescope live_grep<cr>", desc = "Find in all files" },
			{ "<leader>b", "<cmd>Telescope buffers<cr>", desc = "Find open buffer" },
			{ "<leader>r", "<cmd>Telescope registers<cr>", desc = "Show registers" },
		},
	},
	-- colorscheme
	{
		"folke/tokyonight.nvim",
		lazy = false,
		priority = 1000,
		config = function()
			local tokyonight = require("tokyonight")
			tokyonight.setup({
				style = "night", -- storm, night, moon, day
				light_style = "day",
				transparent = false,
				terminal_colors = false,
				styles = {
					comments = { italic = false },
					keywords = { italic = false },
				},
				day_brightness = 0.2,
				hide_inactive_statusline = false,
				dim_inactive = true,
				lualine_bold = true,
			})
		end,
	},
	{
		"navarasu/onedark.nvim",
		lazy = true,
		opts = {
			style = "deep",
			toggle_style_key = "<leader>tt",
			toggle_style_list = { "light", "deep" },
			transparent = true,
			code_style = { comments = "none" },
		},
	},
	-- tab- & bufferline
	-- {
	-- 	"romgrk/barbar.nvim",
	-- 	opts = { animation = false, sidebar_filetypes = { NvimTree = true } },
	-- },
	{
		"akinsho/bufferline.nvim",
		config = function()
			local bufferline = require("bufferline")
			bufferline.setup({
				options = {
					style_preset = {
						bufferline.style_preset.minimal,
						bufferline.style_preset.no_italic,
					},
					always_show_bufferline = false,
				},
			})
		end,
	},
	-- status line
	{
		"nvim-lualine/lualine.nvim",
		opts = {
			options = { theme = "auto" },
			sections = {
				lualine_x = {
					"encoding",
					"fileformat",
					"filetype",
					{
						require("lazy.status").updates,
						cond = require("lazy.status").has_updates,
						color = { fg = "#ff9e64" },
					},
				},
			},
			-- winbar = {
			-- 	lualine_a = {},
			-- 	lualine_b = {},
			-- 	lualine_c = { "filename" },
			-- 	lualine_x = {},
			-- 	lualine_y = {},
			-- 	lualine_z = {},
			-- },
			-- inactive_winbar = {
			-- 	lualine_a = {},
			-- 	lualine_b = {},
			-- 	lualine_c = { "filename" },
			-- 	lualine_x = {},
			-- 	lualine_y = {},
			-- 	lualine_z = {},
			-- },
			extensions = { "lazy", "nvim-tree", "trouble" },
			-- extensions = { "lazy", "toggleterm", "nvim-tree", "trouble" },
		},
	},
	-- Auto-close brackets
	{ "windwp/nvim-autopairs", event = { "VeryLazy" }, config = true },
	-- Indent lines
	-- {
	-- 	"lukas-reineke/indent-blankline.nvim",
	-- 	event = { "BufReadPre", "BufNewFile", "BufWritePre" },
	-- 	main = "ibl",
	-- 	opts = {
	-- 		indent = { char = "·" }, -- "▏"
	-- 		-- exclude = { buftypes = { "terminal", "nofile" } },
	-- 	},
	-- },
	-- commenting
	{ "numToStr/Comment.nvim", event = { "VeryLazy" }, opts = {} },
	-- better user experience for Vim marks
	{
		"chentoast/marks.nvim",
		event = { "BufReadPost", "BufNewFile" },
		opts = { mappings = { next = "mä", prev = "mö", toggle = "mm" } },
	},
	-- git integration
	{
		"lewis6991/gitsigns.nvim",
		event = { "BufReadPre", "BufNewFile", "BufWritePre" },
		opts = {
			trouble = false,
			current_line_blame_opts = { virt_text_pos = "eol" },
			current_line_blame_formatter = " <author_time:%R>, <author_time:%d.%m.%y>",
		},
		keys = {
			{ "gö", "<cmd>Gitsigns prev_hunk<cr>", desc = "Go to previous git change" },
			{ "gä", "<cmd>Gitsigns next_hunk<cr>", desc = "Go to next git change" },
			{ "gB", "<cmd>Gitsigns toggle_current_line_blame<cr>", desc = "Toggle git blame" },
		},
	},
	-- git diff view
	{
		"sindrets/diffview.nvim",
		cmd = { "DiffviewOpen", "DiffviewFileHistory" },
		keys = { { "<leader>d", "<cmd>DiffviewOpen<cr>", desc = "Open git diff view" } },
	},
	-- file tree
	{
		"kyazdani42/nvim-tree.lua",
		cmd = { "NvimTreeFocus", "NvimTreeOpen", "NvimTreeToggle" },
		opts = { update_focused_file = { enable = true } },
		keys = { { "<C-n>", "<cmd>NvimTreeToggle<cr>", desc = "Toggle file tree" } },
	},
	-- file system manipulation
	{
		"stevearc/oil.nvim",
		opts = {
			skip_confirm_for_simple_edits = true,
			keymaps = {
				["<BS>"] = "actions.parent",
				["<Esc>"] = "actions.close",
				["q"] = "actions.close",
				["<C-s>"] = false,
			},
		},
		keys = {
			{
				"-",
				function()
					require("oil").open_float()
				end,
				desc = "Edit parent directory",
			},
		},
		lazy = false,
	},
	-- no delay for escape key mapping
	{
		"max397574/better-escape.nvim",
		event = "InsertEnter",
		opts = {
			timeout = 300,
		},
	},
	-- Multi cursor
	-- {
	-- 	"mg979/vim-visual-multi",
	-- 	event = "VeryLazy",
	-- 	init = function()
	-- 		vim.g.VM_show_warnings = 0
	-- 		vim.g.VM_silent_exit = 1
	-- 		vim.g.VM_quit_after_leaving_insert_mode = 1
	-- 		vim.g.VM_Insert_hl = "DiffText"
	-- 		vim.g.VM_Mono_hl = "DiffText"
	-- 		vim.g.VM_default_mappings = 0
	-- 		vim.g.VM_mouse_mappings = 1
	-- 		vim.g.VM_maps = {
	-- 			["Find Under"] = "<C-d>",
	-- 			["Find Subword Under"] = "<C-d>",
	-- 			["Add Cursor Down"] = "<M-Down>",
	-- 			["Add Cursor Up"] = "<M-Up>",
	-- 			["Add Cursor At Pos"] = "<C-e>",
	-- 			["Visual Cursors"] = "I",
	-- 		}
	-- 	end,
	-- },
	{
		"jake-stewart/multicursor.nvim",
		-- branch = "1.0",
		config = function()
			local mc = require("multicursor-nvim")
			mc.setup()
			local set = vim.keymap.set

			-- Add or skip cursor above/below the main cursor.
			set({ "n", "v" }, "<M-Up>", function()
				mc.lineAddCursor(-1)
			end)
			set({ "n", "v" }, "<M-Down>", function()
				mc.lineAddCursor(1)
			end)
			-- Rotate the main cursor.
			set({ "n", "v" }, "<M-Right>", mc.nextCursor)
			set({ "n", "v" }, "<M-Left>", mc.prevCursor)

			-- Add or skip adding a new cursor by matching word/selection
			set({ "n" }, "<C-d>", "viw")
			set({ "v" }, "<C-d>", function()
				mc.matchAddCursor(1)
			end)
			set({ "n", "v" }, "<C-k><C-d>", function()
				mc.matchSkipCursor(1)
			end)

			-- Add and remove cursors with control + left click.
			set("n", "<c-leftmouse>", mc.handleMouse)

			-- Easy way to add and remove cursors using the main cursor.
			set({ "n", "v" }, "<c-q>", mc.toggleCursor)

			set("n", "<esc>", function()
				if not mc.cursorsEnabled() then
					mc.enableCursors()
				elseif mc.hasCursors() then
					mc.clearCursors()
				else
					vim.cmd("noh")
					-- Default <esc> handler.
				end
			end)

			-- Append/insert for each line of visual selections.
			-- set("v", "I", mc.insertVisual)
			-- set("v", "A", mc.appendVisual)
		end,
	},
	-- Navigate tmux and vim windows with the same commands
	{
		"aserowy/tmux.nvim",
		opts = { copy_sync = { enable = false } },
		keys = {
			{ "<C-h>", "<cmd>lua require('tmux').move_left()<cr>" },
			{ "<C-j>", "<cmd>lua require('tmux').move_down()<cr>" },
			{ "<C-k>", "<cmd>lua require('tmux').move_up()<cr>" },
			{ "<C-l>", "<cmd>lua require('tmux').move_right()<cr>" },
		},
	},
	-- Mark active pane with a colorful border
	{
		"nvim-zh/colorful-winsep.nvim",
		event = "BufEnter", -- TODO: is there a better event? WinNew?
		opts = { highlight = { fg = "#7AA2F7" } },
	},
	-- Install language servers using a GUI via :Mason command
	{
		"williamboman/mason.nvim",
		opts = { ui = { border = "rounded" } },
		dependencies = {
			"jayp0521/mason-null-ls.nvim",
			"williamboman/mason-lspconfig.nvim",
		},
	},
	-- Language server protocol (LSP) plugin
	{
		"neovim/nvim-lspconfig",
		config = function()
			require("plugins.lspconfig")
		end,
		dependencies = {
			-- "ii14/lsp-command", -- access to all lsp features via :Lsp command
			"onsails/lspkind-nvim", -- add pictograms to LSP
			-- show the signature of functions while typing
			-- { "ray-x/lsp_signature.nvim", opts = { hint_enable = false } },
		},
	},
	-- Add features to LSP that do not come from LSP sources (e.g. formatting)
	{
		"nvimtools/none-ls.nvim",
		event = "VeryLazy",
		-- jose-elias-alvarez/null-ls.nvim
		config = function()
			require("plugins.null-ls")
		end,
	},
	-- LSP progress indicator
	{ "j-hui/fidget.nvim", opts = {} },
	-- autocompletion support
	{
		"hrsh7th/nvim-cmp",
		event = "InsertEnter",
		config = function()
			require("plugins.cmp")
		end,
		dependencies = {
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-path",
			"saadparwaiz1/cmp_luasnip",
		},
	},
	-- Snippets
	{
		"L3MON4D3/LuaSnip",
		dependencies = {
			"rafamadriz/friendly-snippets",
			config = function()
				require("luasnip.loaders.from_vscode").lazy_load()
			end,
		},
		lazy = true,
	},
	-- Diagnostics panel
	{
		"folke/trouble.nvim",
		keys = {
			{
				"<leader>xx",
				"<cmd>Trouble diagnostics toggle<cr>",
				desc = "Diagnostics (Trouble)",
			},
			{
				"<leader>xX",
				"<cmd>Trouble diagnostics toggle filter.buf=0<cr>",
				desc = "Buffer Diagnostics (Trouble)",
			},
			{
				"<leader>cs",
				"<cmd>Trouble symbols toggle focus=false<cr>",
				desc = "Symbols (Trouble)",
			},
			{
				"<leader>cl",
				"<cmd>Trouble lsp toggle focus=false win.position=right<cr>",
				desc = "LSP Definitions / references / ... (Trouble)",
			},
			{
				"<leader>xL",
				"<cmd>Trouble loclist toggle<cr>",
				desc = "Location List (Trouble)",
			},
			{
				"<leader>xQ",
				"<cmd>Trouble qflist toggle<cr>",
				desc = "Quickfix List (Trouble)",
			},
		},
		opts = {}, -- for default options, refer to the configuration section for custom setup.
	},
	-- Support for bullet lists
	{
		"dkarter/bullets.vim",
		-- ft = { "markdown", "text" },
		config = function()
			vim.g.bullets_checkbox_markers = " .oOx"
		end,
	},
	-- {
	-- 	"gaoDean/autolist.nvim",
	-- 	ft = {
	-- 		"markdown",
	-- 		"text",
	-- 		"tex",
	-- 		"plaintex",
	-- 	},
	-- 	config = function()
	-- 		local autolist = require("autolist")
	-- 		autolist.setup()
	-- 		autolist.create_mapping_hook("i", "<CR>", autolist.new)
	-- 		autolist.create_mapping_hook("i", "<Tab>", autolist.indent)
	-- 		autolist.create_mapping_hook("i", "<S-Tab>", autolist.indent, "<C-D>")
	-- 		autolist.create_mapping_hook("n", "o", autolist.new)
	-- 		autolist.create_mapping_hook("n", "O", autolist.new_before)
	-- 		autolist.create_mapping_hook("n", "<leader>x", autolist.invert_entry, "")
	-- 	end,
	-- },
	-- show help when entering keyboard shortcuts
	-- { "folke/which-key.nvim", event = "VeryLazy", config = true, keys = { { "<F2>", "<cmd>WhichKey<cr>" } } },
	-- for using Telescope for vim.ui commands
	{ "stevearc/dressing.nvim", event = "VeryLazy" },
	-- Fancy notifications
	{
		"rcarriga/nvim-notify",
		event = "VeryLazy",
		config = function()
			require("notify").setup({ stages = "fade", timeout = 10000, top_down = false, max_width = 80 })
			vim.notify = require("notify")
		end,
	},
	-- a command & keymap palette, TODO: also try cheatsheet.nvim
	{
		"mrjones2014/legendary.nvim",
		cmd = { "Legendary" },
		opts = { select_prompt = "Commands", extensions = { lazy_nvim = true } },
		keys = { { "<F1>", "<cmd>Legendary<cr>", desc = "Open Legendary" } },
	},
	-- write math formulas in plain text
	-- "jbyuki/nabla.nvim",
	-- better wild menu
	-- "gelguy/wilder.nvim",
	-- -- prevent repetitive keypresses of arrow keys
	-- {
	-- 	"ja-ford/delaytrain.nvim",
	-- 	opts = {
	-- 		delay_ms = 1000,
	-- 		grace_period = 1,
	-- 		keys = {
	-- 			-- ["nv"] = { "h", "j", "k", "l" },
	-- 			["n"] = { "<Left>", "<Down>", "<Up>", "<Right>" },
	-- 			-- ["nvi"] = { "<Left>", "<Down>", "<Up>", "<Right>" },
	-- 		},
	-- 	},
	-- },
	-- Better Terminal toggling / more options for the built-in terminal
	{
		"akinsho/toggleterm.nvim",
		opts = {
			persist_mode = false,
			float_opts = { border = "rounded" },
		},
		keys = {
			{
				"<leader>t",
				function()
					require("toggleterm.terminal").Terminal:new():toggle()
				end,
				desc = "Open a new terminal",
			},
			{
				"<leader>T",
				function()
					require("toggleterm.terminal").Terminal:new({ direction = "float", hidden = true }):toggle()
				end,
				desc = "Open a new floating terminal",
			},
		},
	},
	-- mini.nvim library with various features
	{
		"echasnovski/mini.nvim",
		config = function()
			-- require("mini.tabline").setup()
			-- remove buffers but preserve window layout
			require("mini.bufremove").setup()
			-- highlight word under cursor
			require("mini.cursorword").setup()
			-- For putting brackets around selections
			require("mini.surround").setup()
			-- Move lines with key combos
			require("mini.move").setup({
				mappings = {
					left = "<C-Left>",
					right = "<C-Right>",
					down = "<C-Down>",
					up = "<C-Up>",
					line_down = "<C-Down>",
					line_up = "<C-Up>",
				},
			})
		end,
		event = "VeryLazy",
	},
	-- Git client
	-- { "NeogitOrg/neogit", dependencies = "nvim-lua/plenary.nvim", opts = {}, cmd = "Neogit" },

	-- breadcrumbs
	-- {
	-- 	"Bekaboo/dropbar.nvim",
	-- 	event = { "BufReadPost", "BufNewFile" },
	-- },

	-- File names in top right corner
	{ "b0o/incline.nvim", opts = {}, event = { "BufReadPost", "BufNewFile" } },

	-- Scrollbar
	{
		"dstein64/nvim-scrollview",
		event = { "BufReadPost", "BufNewFile" },
		opts = {
			signs_on_startup = { "conflicts", "diagnostics", "marks", "search" },
		},
	},

	-- {
	-- 	"folke/noice.nvim",
	-- 	event = "VeryLazy",
	-- 	opts = {
	-- 		lsp = {
	-- 			-- override markdown rendering so that **cmp** and other plugins use **Treesitter**
	-- 			override = {
	-- 				["vim.lsp.util.convert_input_to_markdown_lines"] = true,
	-- 				["vim.lsp.util.stylize_markdown"] = true,
	-- 				["cmp.entry.get_documentation"] = true,
	-- 			},
	-- 		},
	-- 		-- you can enable a preset for easier configuration
	-- 		presets = {
	-- 			bottom_search = true, -- use a classic bottom cmdline for search
	-- 			command_palette = true, -- position the cmdline and popupmenu together
	-- 			long_message_to_split = true, -- long messages will be sent to a split
	-- 			inc_rename = false, -- enables an input dialog for inc-rename.nvim
	-- 			lsp_doc_border = false, -- add a border to hover docs and signature help
	-- 		},
	-- 		routes = {
	-- 			{
	-- 				filter = {
	-- 					event = "msg_show",
	-- 					kind = "",
	-- 					find = "written",
	-- 				},
	-- 				opts = { skip = true },
	-- 			},
	-- 			{
	-- 				filter = {
	-- 					event = "msg_show",
	-- 					kind = "",
	-- 					find = "geschrieben",
	-- 				},
	-- 				opts = { skip = true },
	-- 			},
	-- 		},
	-- 	},
	-- 	dependencies = {
	-- 		-- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
	-- 		"MunifTanjim/nui.nvim",
	-- 		-- OPTIONAL:
	-- 		--   `nvim-notify` is only needed, if you want to use the notification view.
	-- 		--   If not available, we use `mini` as the fallback
	-- 		"rcarriga/nvim-notify",
	-- 	},
	-- },

	{
		"tris203/precognition.nvim",
		--event = "VeryLazy",
		config = {
			startVisible = false,
			showBlankVirtLine = true,
			-- highlightColor = { link = "Comment" },
			-- hints = {
			--      Caret = { text = "^", prio = 2 },
			--      Dollar = { text = "$", prio = 1 },
			--      MatchingPair = { text = "%", prio = 5 },
			--      Zero = { text = "0", prio = 1 },
			--      w = { text = "w", prio = 10 },
			--      b = { text = "b", prio = 9 },
			--      e = { text = "e", prio = 8 },
			--      W = { text = "W", prio = 7 },
			--      B = { text = "B", prio = 6 },
			--      E = { text = "E", prio = 5 },
			-- },
			-- gutterHints = {
			--     G = { text = "G", prio = 10 },
			--     gg = { text = "gg", prio = 9 },
			--     PrevParagraph = { text = "{", prio = 8 },
			--     NextParagraph = { text = "}", prio = 8 },
			-- },
		},
	},
	-- for toggling boolean values
	{
		"nat-418/boole.nvim",
		config = function()
			require("boole").setup({
				mappings = {
					increment = "gt",
					decrement = "gT",
				},
			})
		end,
	},
	-- end of plugin list
}, {
	-- lazy.nvim configuration
	install = { colorscheme = { "tokyonight-night", "habamax" } },
	ui = { border = "rounded" },
	checker = { enabled = true, notify = false },
	performance = {
		rtp = {
			disabled_plugins = {
				"gzip",
				"netrwPlugin",
				"rplugin",
				"tarPlugin",
				"tohtml",
				"tutor",
				"zipPlugin",
			},
		},
	},
	-- defaults = { lazy = true },
})
